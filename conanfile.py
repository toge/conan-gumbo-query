from conans import ConanFile, CMake, tools

class GumboqueryConan(ConanFile):
    name        = "gumbo-query"
    version     = "20160104"
    license     = "MIT"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-gumbo-query.git"
    description = "c++ library to provide jQuery style api for gumbo library "
    topics      = ("html", "parser", "jquery")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    requires    = "gumbo-parser/[>= 0.10.1]@toge/stable"

    def source(self):
        self.run("git clone https://github.com/lazytiger/gumbo-query/")
        self.run("cd gumbo-query && git checkout 9a269cd94caa32f6d3ee7a203d87bdebc7d38978")

        tools.replace_in_file("gumbo-query/CMakeLists.txt", "project(gumbo_query CXX)", '''project(gumbo_query LANGUAGES C CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''' )


    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_BUILD_TYPE"] = "Release"
        cmake.definitions["Gumbo_static_LIBRARY"] = True
        cmake.configure(source_folder="gumbo-query")
        cmake.build(target="gumbo_query_static")


    def package(self):
        self.copy("*.h", dst="include/gq", src="gumbo-query/src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["gq"]

