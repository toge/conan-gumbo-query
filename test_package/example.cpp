#include <iostream>

// Gumbo query
#include "gumbo.h"

// Gumbo parser
#include "gq/Document.h"
#include "gq/Node.h"
#include "gq/Selection.h"


int main() {
    auto document = CDocument{};
    document.parse("<html></html>");

    return 0;
}
